# frozen_string_literal: true

source 'https://rubygems.org'

git_source(:gitlab) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://gitlab.com/#{repo_name}.git"
end

ruby '>= 2.4.4'

gem 'lexicon-common', gitlab: 'ekylibre/lexicon/lexicon-common', branch: 'master'

# CLI and app core system
gem 'colored', '~> 1.2'
gem 'concurrent-ruby', '~> 1.1'
gem 'dotenv', '~> 2.7'
gem 'dry-container', '~> 0.7.2'
gem 'progress_bar', '~> 1.3'
gem 'thor', '~> 1.1'
gem 'zeitwerk', '~> 2.4'

# Utilities
gem 'activesupport', '~> 6.1'
gem 'aws-sdk-s3', '~> 1.91'
gem 'charlock_holmes', '~> 0.7.7'
gem 'i18n', '~> 1.8'
gem 'json', '~> 2.5'
gem 'json_schemer', '~> 0.2.18'
gem 'nokogiri', '~> 1.11'
gem 'pg', '~> 1.2'
gem 'rest-client', '~> 2.1'
gem 'roo', '~> 2.8'
gem 'roo-xls', '~> 1.2'
gem 'rubyzip', '~> 2.3'
gem 'semantic', '~> 1.6'

# Debug
gem 'pry', '~> 0.13.0'
gem 'pry-byebug'

# Data
gem 'onoma', gitlab: 'ekylibre/onoma', branch: 'master'

# Doc and code quality
gem 'rubocop', '~> 1.11.0', require: false
gem 'yard', '~> 0.9.26', require: false
