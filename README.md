# Lexicon
**Lexicon** is the repo where we gather and normalize all reference data to
feed `lexicon` schema of Ekylibre.

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Installation](#installation)
- [Upgrade](#upgrade)
- [Usage](#usage)
- [How to contribute](#how-to-contribute)
- [Data sources](#data-sources)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Installation
See [doc/INSTALL.md](doc/INSTALL.md)

## Upgrade
See [doc/UPGRADING.md](doc/UPGRADING.md)

## Usage
See [doc/USAGE.md](doc/USAGE.md)

## How to contribute
See [doc/CONTRIBUTING.md](doc/CONTRIBUTING.md)

## Data sources
See [doc/DATASOURCES.md](doc/DATASOURCES.md)