FROM k8s.gcr.io/pause:3.1 AS pause

FROM ruby:2.7-buster

ENV USER=lexicon
ENV UID=1001
ENV GID=1001

COPY --from=pause /pause /pause

RUN mkdir /lexicon && \
    addgroup --gid "$GID" "$USER" && \
    adduser \
        --disabled-password \
        --gecos "" \
        --home /lexicon \
        --ingroup "$USER" \
        --no-create-home \
        --uid "$UID" \
        "$USER" && \
    apt-get update && \
    apt-get -y install postgis postgresql-client p7zip-full pigz libyajl-dev --no-install-recommends && \
    apt-get -y install python3-setuptools python3-dev python3-pip --no-install-recommends && \
    pip3 install wheel && gem install bundler

WORKDIR /lexicon

ADD requirements.txt /lexicon/
RUN pip3 install -r requirements.txt
RUN pip3 install --upgrade requests  
RUN pip3 install -r requirements.txt

ADD Gemfile Gemfile.lock /lexicon/
RUN bundle -j $(nproc)

ADD . /lexicon/
RUN chown -R lexicon:lexicon /lexicon

RUN sha1sum Dockerfile docker-compose.yml Gemfile Gemfile.lock requirements.txt lexicon > build_hash.sha1

USER lexicon

CMD ["/pause"]
